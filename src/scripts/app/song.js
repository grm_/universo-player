window.now = function (data) {
  let info = data.items[0];
  document.querySelector('.current').innerHTML = `${info.artist} - ${info.song}`
  document.title = `${info.artist} - ${info.song} | universo.cl`.toLocaleLowerCase()
}

var jsonp = {
  getJSON: function (url, data, callback) {
    var src = url + (url.indexOf('?') + 1 ? '&' : '?')
    var head = document.getElementsByTagName('head')[0]
    var script = document.createElement('script')
    var params = []
    var param_name = ''

    this.success = callback

    data['callback'] = Date.now()

    for (param_name in data) {
      params.push(param_name + '=' + encodeURIComponent(data[param_name]))
    }

    src += params.join('&')
    script.id = 'get-song'
    script.src = src;

    if (document.getElementById('get-song')) {
      head.removeChild(document.getElementById('get-song'))
    }

    head.appendChild(script)
  },
  success: null
}

module.exports.songInfo = function() {
  jsonp.getJSON('//universo937.digitalproserver.com/json/now.json', {})
}

