import { songInfo } from './song'

var controls = {}
var elements = ['play', 'pause', 'on', 'off', 'stop']
// var src = '//redirector.dps.live/universo/aac/icecast.audio'
var src = '//unlimited11-cl.dps.live/universo/aac/icecast.audio'
var player = document.getElementById('player')
var events = ['abort', 'canplay', 'canplaythrough', 'durationchange', 'emptied', 'ended', 'error', 'loadeddata', 'loadedmetadata', 'loadstart', 'pause', 'play', 'playing', 'ratechange', 'stalled', 'suspend', 'volumechange', 'waiting']

elements.forEach(function (element) {
  controls[element] = document.querySelector('.controls__btn--' + element)
})

var startPlayer = function() {
  if (!player.src) {
    player.src = src
  }

  songInfo()

  var getSongInfo = function() {
    if (status === 'playing') {
      setTimeout(() => {
        songInfo()
        getSongInfo()
      }, 5000);
    }
  }

  let promise = player.play()

  promise
    .then(() => {
      getSongInfo()
    })
    .catch(error => {
      document.body.setAttribute('data-status', 'no-interaction')
    })
}

// event handler
var status = 'stopped'
var onEvent = function (e) {
  switch (e.type) {
    case 'playing':
      status = 'playing'
      break;
    case 'pause':
      status = 'paused'
      break;
    case 'volumechange':
      if (player.muted) {
        document.body.setAttribute('muted', '')
      } else {
        document.body.removeAttribute('muted')
      }
      break;
    case 'suspend':
      status = 'stopped'
      break;
  }
  document.body.setAttribute('data-status', status)
}

// add event listener to audio for all events
for (var i = 0, len = events.length; i < len; i++) {
  player.addEventListener(events[i], onEvent, false)
}

startPlayer()

controls.play.addEventListener('click', function () {
  startPlayer()
})

controls.pause.addEventListener('click', function () {
  player.pause()
})

controls.stop.addEventListener('click', function () {
  player.pause()
  player.currentTime = 0
  document.body.setAttribute('data-status', 'stopped')
})

controls.on.addEventListener('click', function () {
  player.muted = true
})

controls.off.addEventListener('click', function () {
  player.muted = false
});



